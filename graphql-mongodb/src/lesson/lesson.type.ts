import { Field, ID, ObjectType } from '@nestjs/graphql';
import { IsUUID } from 'class-validator';
import { StudentType } from 'src/student/student.type';

@ObjectType('Lesson') //to use this lesson instead of LessonType
export class LessonType {
  @Field((_type) => ID) // is a type that graphQL has
  id: string;
  @Field()
  name: string;
  @Field()
  startDate: string;
  @Field()
  endDate: string;

  @IsUUID('4', { each: true })
  @Field((_type) => [StudentType], { defaultValue: [] })
  students: Array<string>;
}

