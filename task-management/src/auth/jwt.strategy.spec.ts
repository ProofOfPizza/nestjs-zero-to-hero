import { JwtStrategy } from './jwt.strategy';
import { Test } from '@nestjs/testing';
import { UserRepository } from './user.repository';
import { User } from './user.entity';
import { UnauthorizedException } from '@nestjs/common';

describe('JwtStrategy', () => {
  const mockUserRepository = { findOne: jest.fn() };
  let jwtStrategy: JwtStrategy;
  let userRepository: any;

  let payload = { username: 'testUserA' };
  let user: User;

  beforeEach(async () => {
    let module = await Test.createTestingModule({
      providers: [
        JwtStrategy,
        { provide: UserRepository, useFactory: () => mockUserRepository },
      ],
    }).compile();

    jwtStrategy = module.get<JwtStrategy>(JwtStrategy);
    userRepository = module.get<UserRepository>(UserRepository);

    user = new User();
    user.username = 'testUser';
  });

  it('returns the user when found', async () => {
    userRepository.findOne.mockResolvedValue(user);
    const result = await jwtStrategy.validate(payload);
    expect(userRepository.findOne).toHaveBeenCalledWith({
      username: 'testUserA',
    });
    expect(result).toEqual(user);
  });

  it('throws and unauthorized exception if user is not found', async () => {
    userRepository.findOne.mockResolvedValue(false);
    await expect(jwtStrategy.validate(payload)).rejects.toThrow(
      UnauthorizedException,
    );
    expect(userRepository.findOne).toHaveBeenCalledWith({
      username: 'testUserA',
    });
  });
});

