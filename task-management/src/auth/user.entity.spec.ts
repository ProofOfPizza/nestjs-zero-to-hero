import { User } from './user.entity';
import * as bcrypt from 'bcrypt';

describe('userEntity', () => {
  describe('validPassword', () => {
    let user: User;
    let spy: any;

    beforeEach(() => {
      user = new User();
      user.salt = 'testSalt';
      user.password = 'actualPasswordHash';
      spy = jest.spyOn(bcrypt, 'hash');
    });
    it('returns true if password is valid', async () => {
      spy.mockResolvedValue('actualPasswordHash');
      const result = await user.validPassword('correctPassword');
      expect(spy).toHaveBeenCalledWith('correctPassword', user.salt);
      expect(result).toEqual(true);
    });

    it('returns false if password is invalid', async () => {
      spy.mockResolvedValue('someFalsePasswordHash');
      const result = await user.validPassword('incorrectPassword');
      expect(spy).toHaveBeenCalledWith('incorrectPassword', user.salt);
      expect(result).toEqual(false);
    });
  });
});

