import * as bcrypt from 'bcrypt';
import {
  ConflictException,
  InternalServerErrorException,
} from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { User } from './user.entity';
import { UserRepository } from './user.repository';
import { has } from 'config';

describe('UserRepository', () => {
  let userRepository: UserRepository;

  let authCredentialsDto: AuthCredentialsDto = {
    username: 'usahh',
    password: 'passworD12!@',
  };

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [UserRepository],
    }).compile();

    userRepository = module.get<UserRepository>(UserRepository);
  });

  describe('signUp', () => {
    let save: any;
    beforeEach(() => {
      save = jest.fn();
      userRepository.create = jest.fn().mockReturnValue({ save });
    });

    it('creates a user', () => {
      save.mockResolvedValue(true);
      expect(userRepository.signUp(authCredentialsDto)).resolves.not.toThrow();
    });

    it('throws a conflict exception if user already exist', async () => {
      save.mockRejectedValue({
        code: '23505',
      });
      await expect(userRepository.signUp(authCredentialsDto)).rejects.toThrow(
        ConflictException,
      );
    });
    it('throws an internal server error exception in case of another error', async () => {
      save.mockRejectedValue({
        code: '666',
      });
      await expect(userRepository.signUp(authCredentialsDto)).rejects.toThrow(
        InternalServerErrorException,
      );
    });
  });

  describe('validatePassword', () => {
    let user: User;

    beforeEach(async () => {
      userRepository.findOne = jest.fn();
      user = new User();
      user.username = 'testuser';
      user.validPassword = jest.fn();
    });
    it('it returns the username', async () => {
      (userRepository.findOne as jest.Mock).mockResolvedValue(user);
      (user.validPassword as jest.Mock).mockResolvedValue(true);
      const result = await userRepository.validatePassword(authCredentialsDto);
      expect(result).toEqual('testuser');
    });

    it('returns null as the user can not be found', async () => {
      (userRepository.findOne as jest.Mock).mockResolvedValue(null);
      const result = await userRepository.validatePassword(authCredentialsDto);
      expect(result).toEqual(null);
    });

    it('returns null as the password is invalid', async () => {
      (userRepository.findOne as jest.Mock).mockResolvedValue(user);
      (user.validPassword as jest.Mock).mockResolvedValue(false);
      const result = await userRepository.validatePassword(authCredentialsDto);
      expect(result).toEqual(null);
      expect(user.validPassword).toHaveBeenCalled();
    });
  });

  describe('hashPassword', () => {
    it('calls bcrypt.hash to generate a hash', async () => {
      const spy = jest.spyOn(bcrypt, 'hash');
      spy.mockResolvedValue('testHash');
      // bcrypt.hash = jest.fn().mockResolvedValue('testHash');
      const result = await userRepository['hashPassword'](
        'password123',
        'salt456',
      );
      expect(result).toEqual('testHash');
      expect(spy).toHaveBeenCalledWith('password123', 'salt456');
    });
  });
});

