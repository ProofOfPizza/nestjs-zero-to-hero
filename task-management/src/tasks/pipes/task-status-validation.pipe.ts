import {
  ArgumentMetadata,
  BadRequestException,
  PipeTransform,
} from '@nestjs/common';
import { TaskStatus } from '../task-status.enum';

export class TaskStatusValidationPipe implements PipeTransform {
  readonly allowedStatuses = [
    TaskStatus.DONE,
    TaskStatus.OPEN,
    TaskStatus.IN_PROGRESS,
  ];
  transform(value: any, _metadata: ArgumentMetadata) {
    const valueUp = value.toUpperCase();
    if (!this.isStatusAllowed(valueUp)) {
      throw new BadRequestException(`${value} is not an allowed status`);
    }
    return valueUp;
  }
  private isStatusAllowed(status: any): boolean {
    return this.allowedStatuses.indexOf(status) > 0;
  }
}

