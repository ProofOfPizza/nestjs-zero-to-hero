import {
  Body,
  Controller,
  Delete,
  Get,
  Logger,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Query,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from '../auth/get-user.decorator';
import { User } from '../auth/user.entity';
import { CreateTaskDto } from './dto/create-task.dto';
import { GetTasksFilterDto } from './dto/get-tasks-filter.dto';
import { TaskStatusValidationPipe } from './pipes/task-status-validation.pipe';
import { TaskStatus } from './task-status.enum';
import { Task } from './task.entity';
import { TasksService } from './tasks.service';

@Controller('tasks')
@UseGuards(AuthGuard())
export class TasksController {
  private Logger = new Logger('TasksController');
  constructor(private tasksService: TasksService) {}

  @Get()
  getTasks(
    @Query(ValidationPipe)
    getTasksFilterDto: GetTasksFilterDto,
    @GetUser()
    user: User,
  ): Promise<Array<Task>> {
    this.Logger.verbose(
      `User ${
        user.id
      } retrieving all tasks. GetTasksFilterDto: ${JSON.stringify(
        getTasksFilterDto,
      )}`,
    );
    return this.tasksService.getTasks(getTasksFilterDto, user);
  }

  @Post()
  @UsePipes(ValidationPipe)
  createTask(
    @Body() createTaskDto: CreateTaskDto,
    @GetUser()
    user: User,
  ): Promise<Task> {
    this.Logger.verbose(
      `User ${user.id} creating a task. createTaskFilterDto: ${JSON.stringify(
        createTaskDto,
      )}`,
    );
    return this.tasksService.createTask(createTaskDto, user);
  }

  @Get(':id')
  getTaskById(
    @Param('id', ParseIntPipe) id: number,
    @GetUser()
    user: User,
  ): Promise<Task> {
    return this.tasksService.getTaskById(id, user);
  }

  @Delete(':id')
  deleteTask(
    @Param('id', ParseIntPipe)
    id: number,
    @GetUser()
    user: User,
  ): Promise<void> {
    return this.tasksService.deleteTask(id, user);
  }

  @Patch(':id/status')
  updateTaskStatus(
    @Param('id', ParseIntPipe) id: number,
    @Body('status', TaskStatusValidationPipe) newStatus: TaskStatus,
    @GetUser()
    user: User,
  ): Promise<Task> {
    return this.tasksService.updateTaskStatus(id, newStatus, user);
  }
}

