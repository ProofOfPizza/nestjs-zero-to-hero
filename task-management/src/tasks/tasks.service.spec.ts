import { NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { User } from '../auth/user.entity';
import { CreateTaskDto } from './dto/create-task.dto';
import { GetTasksFilterDto } from './dto/get-tasks-filter.dto';
import { TaskStatus } from './task-status.enum';
import { TaskRepository } from './task.repository';
import { TasksService } from './tasks.service';

describe('TasksService', () => {
  let tasksService: TasksService;
  let taskRepository: TaskRepository;

  const mockUser: User = new User();
  mockUser.username = 'juanita';
  mockUser.id = 1;

  const mockTaskRepository = {
    getTasks: jest.fn(),
    findOne: jest.fn(),
    createTask: jest.fn(),
    delete: jest.fn(),
    save: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TasksService,
        { provide: TaskRepository, useFactory: () => mockTaskRepository },
      ],
    }).compile();

    tasksService = module.get<TasksService>(TasksService);
    taskRepository = module.get<TaskRepository>(TaskRepository);
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(tasksService).toBeDefined();
  });

  describe('get tasks', () => {
    it('get all tasks from repository', async () => {
      const filters: GetTasksFilterDto = {
        status: TaskStatus.OPEN,
        search: 'some string',
      };
      (taskRepository.getTasks as jest.Mock).mockResolvedValue('result');

      expect(taskRepository.getTasks).not.toHaveBeenCalled();
      const result = await tasksService.getTasks(filters, mockUser);
      expect(taskRepository.getTasks).toHaveBeenCalledWith(filters, mockUser);
      expect(result).toEqual('result');
    });
  });

  describe('get tasks by id', () => {
    it('calls taskRepository and finds one succesfully', async () => {
      const mockTask = { title: 't', description: 'd' };
      (taskRepository.findOne as jest.Mock).mockResolvedValue(mockTask);
      const result = await tasksService.getTaskById(7, mockUser);
      expect(taskRepository.findOne).toHaveBeenCalledWith({
        where: { id: 7, userId: mockUser.id },
      });
      expect(result).toEqual({ title: 't', description: 'd' });
    });

    it('throws an exception when id is not found', () => {
      (taskRepository.findOne as jest.Mock).mockImplementation(() => {
        throw new NotFoundException();
      });
      expect(tasksService.getTaskById(8, mockUser)).rejects.toThrow(
        NotFoundException,
      );
    });
  });

  describe('create tasks', () => {
    it('creates a task and returns it', async () => {
      const mockTask = { title: 't', description: 'd' };
      (taskRepository.createTask as jest.Mock).mockResolvedValue(mockTask);
      const mockCreateTaskDto: CreateTaskDto = {
        title: 'call mom',
        description: "it's her birthday",
      };
      expect(taskRepository.createTask).not.toHaveBeenCalled();
      const result = await tasksService.createTask(mockCreateTaskDto, mockUser);
      expect(taskRepository.createTask).toHaveBeenCalledWith(
        mockCreateTaskDto,
        mockUser,
      );
      expect(result).toEqual(mockTask);
    });
  });

  describe('delete task', () => {
    it('deletes a task when found succesfully', async () => {
      (taskRepository.delete as jest.Mock).mockResolvedValue({ affected: 1 });
      expect(taskRepository.delete).not.toHaveBeenCalled();
      expect(tasksService.deleteTask(5, mockUser)).resolves.not.toThrow();
      expect(taskRepository.delete).toHaveBeenCalledWith({
        id: 5,
        userId: mockUser.id,
      });
    });

    it('throws an exception when the task is not found', () => {
      (taskRepository.delete as jest.Mock).mockResolvedValue({ affected: 0 });
      expect(taskRepository.delete).not.toHaveBeenCalled();
      expect(tasksService.deleteTask(5, mockUser)).rejects.toThrow(
        NotFoundException,
      );
      expect(taskRepository.delete).toHaveBeenCalledWith({
        id: 5,
        userId: mockUser.id,
      });
    });
  });

  describe('update tasks', () => {
    it('updates the task and return the updated task', async () => {
      const updatedTask = {
        title: 't',
        description: 'd',
        status: TaskStatus.OPEN,
        save: jest.fn().mockResolvedValue(true),
      };
      tasksService.getTaskById = jest.fn().mockResolvedValue(updatedTask);
      expect(taskRepository.save).not.toHaveBeenCalled();
      const result = await tasksService.updateTaskStatus(
        7,
        TaskStatus.IN_PROGRESS,
        mockUser,
      );
      expect(updatedTask.save).toHaveBeenCalled();
      expect(result.status).toEqual(updatedTask.status);
    });

    // it('throws and exception if the status were not correct', async () => {
    //   const updatedTask = {
    //     title: 't',
    //     description: 'd',
    //     status: TaskStatus.OPEN,
    //   };
    //   expect(taskRepository.save).not.toHaveBeenCalled();
    //   await expect(
    //     tasksService.updateTaskStatus(7, 'TaskStatus.UNREAL_STATUS', mockUser),
    //   ).rejects.toThrow();
    //   expect(taskRepository.save).not.toHaveBeenCalled();
    // });
  });
});

